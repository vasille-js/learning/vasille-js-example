import {App} from "vasille";
import { Page } from "./components/Page";



export class MyApp extends App {


    constructor () {
        super(document.body, {debugUi: true});
    }



    $compose () {
        super.$compose();

        this.$create(new Page);
    }
}
