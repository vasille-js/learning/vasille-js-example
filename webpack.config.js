const path = require ("path");

module.exports = {
    mode : "development",
    entry : "./src/main.js",
    output : {
        filename : "app.js",
        path : path.resolve (__dirname, "dist")
    },
    stats : {
        colors : true
    },
    devtool : "source-map",
    optimization : {
        minimize : true
    },
    watch : false,
    watchOptions : {
        ignored : /node_modules/,
        aggregateTimeout : 10000
    },
    devServer : {
        contentBase : path.join (__dirname, 'dist'),
        port : 3000
    }
};
